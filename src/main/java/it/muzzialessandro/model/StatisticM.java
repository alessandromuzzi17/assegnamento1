package it.muzzialessandro.model;

import manifold.ext.props.rt.api.val;
import manifold.ext.props.rt.api.var;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The {@code StatisticM} class is a model class for a client
 */
public class StatisticM implements Serializable {

    //region Properties

    @val int clientID;
    @val int messagesToSend;
    @var long executionTimeMS;
    @var int sentMessages;
    @var int resentMessages;
    @var AtomicInteger receivedMessages;
    @var AtomicInteger lostMessages;

    //endregion

    public StatisticM(int clientID, int messagesToSend) {
        //region Init Fields

        this.clientID = clientID;
        this.messagesToSend = messagesToSend;
        this.executionTimeMS = 0;
        this.sentMessages = 0;
        this.resentMessages = 0;
        this.receivedMessages = new AtomicInteger(0);
        this.lostMessages = new AtomicInteger(0);

        //endregion
    }

    //region Public Methods

    public void updateSentMsgForNClients(int n) {
        this.sentMessages *= n;
        this.resentMessages *= n;
    }

    //endregion
}
