package it.muzzialessandro.model;

import manifold.ext.props.rt.api.val;

import java.io.Serial;
import java.io.Serializable;

/**
 * The class {@code Request} provides a simplified model of a request message.
 **/
public class RequestM implements Serializable {

    /**
     * Request codes, needed to identify the request type
     */
    public enum RequestCode {
        REGISTRATION,
        NETWORK_INFO,
        RESEND_MESSAGE,
        MESSAGE_TRANSMISSION_END,
        STATISTICS
    }

    @Serial
    private static final long serialVersionUID = 1L;

    //region Properties

    @val int senderID;
    @val RequestCode code;
    @val Object obj;

    //endregion


    /**
     * Class constructor.
     *
     * @param senderID  The sender ID
     * @param code      The code that identifies the request type
     * @param obj       The payload
     **/
    public RequestM(int senderID, RequestCode code, Serializable obj) {
        this.senderID = senderID;
        this.code = code;
        this.obj = obj;
    }
}
