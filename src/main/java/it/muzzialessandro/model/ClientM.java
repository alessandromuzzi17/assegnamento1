package it.muzzialessandro.model;

import manifold.ext.props.rt.api.var;

import java.io.Serializable;

/**
 * The {@code ClientM} class is a model class for a client
 */
public class ClientM implements Serializable {

    //region Properties

    // this could have been done with a record as well, but properties
    // allow for a cleaner access (in terms of syntax) and a custom getter/setter implementation, if needed
    @var int id;
    @var String address;
    @var int port;

    //endregion

    public ClientM(int id, String address, int port) {
        this.id = id;
        this.address = address;
        this.port = port;
    }
}
