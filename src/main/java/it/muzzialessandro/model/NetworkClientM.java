package it.muzzialessandro.model;

import it.muzzialessandro.manager.ErrorManager;
import it.muzzialessandro.manager.PacketManager;
import manifold.ext.props.rt.api.var;

import java.io.*;
import java.net.Socket;


/**
 * The {@code NetworkClientM} class is a model class for a network client,
 * this means to be a client model with some network tools
 */
public class NetworkClientM extends ClientM {

    //region Properties

    @var Socket socket;
    @var ObjectOutputStream outputStream;

    //endregion

    /**
     * Class constructor.
     *
     * @param clientM   The client model of this network client
     */
    public NetworkClientM(final ClientM clientM) {
        super(clientM.id, clientM.address, clientM.port);
    }

    //region Public Methods

    /**
     * Aims to correctly close the socket
     */
    public synchronized void closeSocket() {
        try {
            this.socket.close();
        } catch (IOException e) {
            ErrorManager.raiseError(25, "Error closing client personal socket.", e);
        }
    }

    //region Packets Management Methods

    /**
     * Sends a new request with the given code to this network client
     *
     * @param senderID      The sender code
     * @param requestCode   The request code
     * @return              True if the request has been sent, False otherwise
     */
    public boolean sendRequest(int senderID, RequestM.RequestCode requestCode) {
        return sendRequest(senderID, requestCode, null);
    }

    /**
     * Sends a new request with the given code and payload to this network client
     *
     * @param senderID      The sender code
     * @param requestCode   The request code
     * @param payload       The request payload
     * @return              True if the request has been sent, False otherwise
     */
    public boolean sendRequest(int senderID, RequestM.RequestCode requestCode, Serializable payload) {
        if(this.socket == null || this.socket.isClosed() || this.outputStream == null)
            return false;

        PacketManager.sendRequest(this.outputStream, senderID, requestCode, payload);
        return true;
    }

    /**
     * Sends a new response with the given codes to this network client
     *
     * @param senderID      The sender code
     * @param requestCode   The request code
     * @param responseCode  The response code
     * @return              True if the request has been sent, False otherwise
     */
    public boolean sendResponse(int senderID, RequestM.RequestCode requestCode, ResponseM.ResponseCode responseCode) {
        return sendResponse(senderID, requestCode, responseCode, null);
    }

    /**
     * Sends a new response with the given codes to this network client
     *
     * @param senderID      The sender code
     * @param requestCode   The request code
     * @param responseCode  The response code
     * @return              True if the request has been sent, False otherwise
     */
    public boolean sendResponse(int senderID, RequestM.RequestCode requestCode, ResponseM.ResponseCode responseCode, Serializable obj) {
        if(this.socket == null || this.socket.isClosed() || this.outputStream == null)
            return false;

        PacketManager.sendResponse(this.outputStream, senderID, requestCode, responseCode, obj);
        return true;
    }

    /**
     * Sends a new message with the given ID to this network client
     *
     * @param senderID      The sender code
     * @param messageID     The message ID
     * @return              True if the request has been sent, False otherwise
     */
    public boolean sendMessage(int senderID, int messageID) {
        if(this.socket == null || this.socket.isClosed() || this.outputStream == null)
            return false;

        PacketManager.sendMessage(this.outputStream, senderID, messageID);
        return true;
    }

    //endregion

    //endregion
}
