package it.muzzialessandro.model;

import manifold.ext.props.rt.api.var;

import java.io.Serializable;

/**
 * The {@code MessageM} class is a model class for a message
 */
public class MessageM implements Serializable {

    @var int senderID;
    @var int messageID;

    /**
     * Class constructor.
     *
     * @param senderID      The sender of the message
     * @param messageID     The ID of the message
     */
    public MessageM(int senderID, int messageID) {
        this.senderID = senderID;
        this.messageID = messageID;
    }
}
