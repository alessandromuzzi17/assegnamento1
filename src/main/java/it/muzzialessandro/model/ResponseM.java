package it.muzzialessandro.model;

import it.muzzialessandro.model.RequestM.RequestCode;

import manifold.ext.props.rt.api.val;

import java.io.Serial;
import java.io.Serializable;

/**
 * The class {@code Response} provides a simplified model of a response message.
 **/
public class ResponseM implements Serializable {

    /**
     * Response codes, needed to identify the request result
     */
    public enum ResponseCode {
        OK,
        ERROR
    }

    @Serial
    private static final long serialVersionUID = 1L;

    //region Properties

    @val int senderID;
    @val RequestCode requestCode;
    @val ResponseCode responseCode;
    @val String message;
    @val Object payload;

    //endregion

    /**
     * Class constructor.
     *
     * @param senderID  The sender ID
     * @param reqCode   The correspondent request code
     * @param resCode   The response code
     **/
    public ResponseM(int senderID, RequestCode reqCode, ResponseCode resCode) {
        this(senderID, reqCode, resCode, "");
    }

    /**
     * Class constructor.
     *
     * @param senderID  The sender ID
     * @param reqCode   The correspondent request code
     * @param resCode   The response code
     * @param message   The response message
     **/
    public ResponseM(int senderID, RequestCode reqCode, ResponseCode resCode, String message) {
        this(senderID, reqCode, resCode, message, null);
    }

    /**
     * Class constructor.
     *
     * @param senderID  The sender ID
     * @param reqCode   The correspondent request code
     * @param resCode   The response code
     * @param payload   The response payload
     **/
    public ResponseM(int senderID, RequestCode reqCode, ResponseCode resCode, Serializable payload) {
        this(senderID, reqCode, resCode, "", payload);
    }

    /**
     * Class constructor.
     *
     * @param senderID  The sender ID
     * @param reqCode   The correspondent request code
     * @param resCode   The response code
     * @param message   The response message
     * @param payload   The response payload
     **/
    public ResponseM(int senderID, RequestCode reqCode, ResponseCode resCode, String message, Serializable payload) {
        this.senderID = senderID;
        this.requestCode = reqCode;
        this.responseCode = resCode;
        this.message = message;
        this.payload = payload;
    }
}
