package it.muzzialessandro;

import it.muzzialessandro.manager.ConfigurationManager;
import it.muzzialessandro.manager.ErrorManager;
import it.muzzialessandro.manager.LogManager;
import it.muzzialessandro.model.*;
import it.muzzialessandro.model.RequestM.RequestCode;
import it.muzzialessandro.model.ResponseM.ResponseCode;
import it.muzzialessandro.manager.PacketManager;
import manifold.ext.props.rt.api.val;
import manifold.ext.props.rt.api.var;

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class ClientNode {

    //region Private Fields

    private static final int CORE_POOL = 9;
    private static final int MAX_POOL = 100;
    private static final long IDLE_TIME = 5000;
    private static final int SENDER_PORT_OFFSET = 100;
    private static final int SENDER_PORT_DIVIDER = 10;

    private final Socket masterSocket;
    private final ObjectOutputStream masterOs;
    private ObjectInputStream masterIs;
    private final int clientBasePort;
    private final Random randomGenerator;
    private final int messagesToSend;
    private final HashMap<Integer, NetworkClientM> networkClients;
    private final AtomicInteger transmissionEndReceivedResponses;
    private final StatisticM stats;

    //endregion

    //region Properties

    @val ThreadPoolExecutor receiveSocketPool;
    @var ServerSocket receiveSocket;
    @val ClientM clientM;
    @val float packetLossPercentage;
    @var AtomicInteger lostMessageID;
    @var boolean isClosing;

    //endregion


    /**
     * Class constructor.
     *
     * @param xmlConfigPath The XML configuration file path
     * @throws Exception If the creation of one of the sockets fails.
     */
    public ClientNode(String xmlConfigPath) throws Exception {
        this(xmlConfigPath, -1);
    }

    /**
     * Class constructor.
     *
     * @param xmlConfigPath The XML configuration file path
     * @param id            The client ID
     * @throws Exception If the creation of one of the sockets fails.
     */
    public ClientNode(String xmlConfigPath, int id) throws Exception {

        //region Init Config

        ConfigurationManager configManager = ConfigurationManager.importConfigurationFromFile(xmlConfigPath);

        String masterAddress = configManager.getFromConfig("master_address");
        int masterPort = Integer.parseInt(configManager.getFromConfig("master_port"));
        this.clientBasePort = Integer.parseInt(configManager.getFromConfig("client_base_port"));
        int clientID = id;
        if(clientID == -1) {
            if(configManager.isDefined("client_id")) {
                clientID = Integer.parseInt(configManager.getFromConfig("client_id"));
            } else {
                ErrorManager.raiseError(26, "Can't retrieve client ID. Client ID must be specified in config file or as argument.");
            }
        }
        int messagesToSend = Integer.parseInt(configManager.getFromConfig("messages_to_send"));
        this.packetLossPercentage = Float.parseFloat(configManager.getFromConfig("packet_loss_percentage"));

        //endregion

        //region Init Fields

        this.clientM = new ClientM(clientID, Inet4Address.getLocalHost().getHostAddress(), this.clientBasePort + clientID);
        this.stats = new StatisticM(this.clientM.id, messagesToSend);
        this.randomGenerator = new Random(this.clientM.id);
        this.masterSocket = new Socket(masterAddress, masterPort);
        this.masterOs = new ObjectOutputStream(this.masterSocket.getOutputStream());
        this.messagesToSend = messagesToSend;
        this.networkClients = new HashMap<>();
        this.lostMessageID =  new AtomicInteger(-1);
        this.transmissionEndReceivedResponses = new AtomicInteger(0);
        this.isClosing = false;

        //endregion

        //region Init Receive Sockets

        this.receiveSocketPool = new ThreadPoolExecutor(CORE_POOL,
                MAX_POOL,
                IDLE_TIME,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>());

        if(this.clientM.id > 0) {
            this.receiveSocket = new ServerSocket(this.clientM.port);
        }

        //endregion
    }

    /**
     * Runs the client code.
     **/
    public void run() {
        try {
            //region Client Registration

            PacketManager.sendRequest(masterOs, this.clientM.id, RequestCode.REGISTRATION, this.clientM);

            this.masterIs = new ObjectInputStream(this.masterSocket.getInputStream());
            Object objRes = masterIs.readObject();

            if (objRes instanceof ResponseM) {

                ResponseM res = (ResponseM) objRes;
                if (res.responseCode != ResponseCode.OK) {
                    ErrorManager.raiseError(15, "Server rejected registration with message: ${res.message}");
                    return;
                }
            } else {
                ErrorManager.raiseError(16, "Server response is invalid.");
                return;
            }

            //endregion

            // wait for other clients list from master
            LogManager.i("Waiting for server start command...");
            Object infoObj = masterIs.readObject();

            if (!(infoObj instanceof RequestM) || ((RequestM) infoObj).code != RequestCode.NETWORK_INFO) {
                ErrorManager.raiseError(20, "Server's network info are invalid.");
                return;
            }

            // saving init time for time measurement
            final long initTime = System.currentTimeMillis();

            // retrieve the network info given from the master
            @SuppressWarnings("unchecked")  // I am sure about what I cast
            HashMap<Integer, ClientM> otherClients = (HashMap<Integer, ClientM>) ((RequestM) infoObj).obj;
            otherClients.forEach((id, client) -> {
                if (id != this.clientM.id)
                    this.networkClients.put(id, new NetworkClientM(client));
            });

            LogManager.i("Waiting for incoming clients...");

            //region Receive Sockets Management

            // accept incoming socket request only from ID's lower than mine,
            // since before accept() I have no way to ensure that the client ID is lower than mine
            // let's just assume that other clients will perform a connection only on IDs grater than their,
            // so I just need to accept m clients with ID lower than mine
            while (receiveSocketPool.getTaskCount() < this.clientM.id) {
                try {
                    // here I'm sure that receive socket is != null
                    Socket s = this.receiveSocket.accept();
                    LogManager.i("New client arrived. There are ${receiveSocketPool.getTaskCount() + 1} clients at the moment.");

                    NetworkClientM netClient = this.networkClients.get((s.getPort() - this.clientBasePort) % SENDER_PORT_DIVIDER);
                    netClient.socket = s;
                    netClient.outputStream = new ObjectOutputStream(new BufferedOutputStream(netClient.socket.getOutputStream()));
                    this.receiveSocketPool.execute(new ClientReceiveThread(this, netClient, this.messagesToSend, this.stats));
                } catch (Exception e) {
                    ErrorManager.raiseError(27, "Couldn't correctly accept the new client.", e);
                }
            }

            // open sockets with network clients with ID bigger than mine
            try {
                for(int i = this.clientM.id+1; i <= this.networkClients.size(); i++) {
                    NetworkClientM netClient = this.networkClients.get(i);
                    LogManager.d("INFO: about to bind on port " + (this.clientM.port + ClientNode.SENDER_PORT_OFFSET*i));
                    netClient.socket = new Socket(netClient.address,
                            netClient.port,
                            Inet4Address.getLocalHost(),
                            this.clientM.port + ClientNode.SENDER_PORT_OFFSET*i);
                    netClient.outputStream = new ObjectOutputStream(new BufferedOutputStream(netClient.socket.getOutputStream()));

                    this.receiveSocketPool.execute(new ClientReceiveThread(this, netClient, this.messagesToSend, this.stats));
                }
            } catch (IOException e) {
                ErrorManager.raiseError(29, "Couldn't correctly create the socket for the network client.", e);
            }

            //endregion

            // send all the messages
            this.sendMessages();

            //region Statistics Report

            this.stats.executionTimeMS = System.currentTimeMillis() - initTime;
            // update send-related fields for all the clients I sent the message for
            this.stats.updateSentMsgForNClients(this.networkClients.size());
            this.sendStats();

            //endregion

            this.closeNetworking();

            masterSocket.close();

        } catch (Exception e) {
            ErrorManager.raiseError(28, "Generic error from ClientNode.run().", e);
        }
    }

    //region Private Methods

    private void sendMessages() {

        //region Algorithm Abstract

        // while I haven't send correctly all the messages
        //      for each client
        //          if there are no lost messages (it means to looking for the lostMessageID that has value -1)
        //              send i-message
        //          else
        //              set i equal to the lostMessageID while resetting it
        //
        //      ask if everything ok, if so
        //          terminate and exit
        //      else
        //          set i equal to the lostMessageID while resetting it

        //endregion

        try {
            int i = 1;

            LogManager.i("Starting to send ${this.messagesToSend} messages to each client.");
            do {
                while (i <= this.messagesToSend) {
                    // check if everything is ok and I can send the i-message
                    if(this.lostMessageID.get() == -1) {
                        for(NetworkClientM netClient : this.networkClients.values()) {
                            if(!netClient.sendMessage(this.clientM.id, i)) {
                                ErrorManager.raiseError(39, "Unable so send message #$i to client ${netClient.id}");
                            }
                        }
                        i++;
                    } else {
                        int lostMsgID = this.lostMessageID.getAndSet(-1);
                        this.stats.resentMessages += i - lostMsgID;
                        i = lostMsgID;
                    }
                }
                LogManager.d("All packets sent, theoretically.");

                // now all messages have been theoretically sent, let's check if it's all ok
                // eventually someone may have lost one of the latest messages,
                // an end confirm request is needed in case someone has lost the last message
                this.transmissionEndReceivedResponses.set(0);
                this.networkClients.forEach((id, netClient) ->{
                    if(netClient.sendRequest(this.clientM.id, RequestCode.MESSAGE_TRANSMISSION_END)) {
                        this.transmissionEndReceivedResponses.incrementAndGet();
                    }
                });

                // wait for updates from receive thread
                synchronized (this.transmissionEndReceivedResponses) {
                    this.transmissionEndReceivedResponses.wait();
                }

                // check if everyone has received all the messages
                int lostMsgID = this.lostMessageID.getAndSet(-1);
                if(lostMsgID != -1) {
                    this.stats.resentMessages += i - lostMsgID;
                    i = lostMsgID;
                    LogManager.d("Someone has lost messages from #"+ i +", resending them now.");
                }

            } while (i <= this.messagesToSend);

            // when it exits the while() loop, 'i' is ready for the next message,
            // so it's grater by one respect to the effectively sent messages
            this.stats.sentMessages = i - 1;
            LogManager.i("Received client confirms. All packets sent.");

        } catch (Exception e) {
            ErrorManager.raiseError(22, "Generic error from ClientSendThread.run().", e);
        }
    }

    private void sendStats() {
        PacketManager.sendRequest(this.masterOs, this.clientM.id, RequestCode.STATISTICS, this.stats);

        try {
            Object infoObj = masterIs.readObject();
            if (!(infoObj instanceof ResponseM) || ((ResponseM) infoObj).requestCode != RequestCode.STATISTICS) {
                ErrorManager.raiseError(20, "Server's response for statistics is invalid.");
                return;
            }

            if(((ResponseM) infoObj).responseCode == ResponseCode.ERROR) {
                ErrorManager.raiseError(43, "Server reported error on sent statistics.");
            }
        } catch (IOException|ClassNotFoundException e) {
            ErrorManager.raiseError(42, "Bad response from server while waiting for response to statistics.", e);
        }

    }

    //endregion

    //region Public Methods

    /**
     * Closes the server execution.
     **/
    public void closeNetworking()
    {
        try
        {
            LogManager.i("All done, closing this client.");

            this.isClosing = true;
            this.networkClients.forEach((id, netClient) -> netClient.closeSocket());

            this.receiveSocketPool.shutdown();

            try {
                //noinspection ResultOfMethodCallIgnored
                this.receiveSocketPool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
            } catch (InterruptedException e) {
                ErrorManager.raiseError(33, "Program has been interrupted while waiting for receiving threads to complete.", e);
            }

            if(this.receiveSocket != null) {
                this.receiveSocket.close();
            }
        }
        catch (Exception e)
        {
            ErrorManager.raiseError(41, "Unable to shut down this node.", e);
        }
    }

    /**
     * Manages the resend requests for lost messages
     *
     * @param msgID     The ID of the message to resend
     */
    public void requestMessageResend(int msgID) {
        //I have to keep this ID updated with the lower request ID:
        // in case multiple requests arrive while I try to start resending,
        // if I keep only the lower ID I'm sure I will serve all the requesters
        // sending the lowest possible number of messages
        this.lostMessageID.updateAndGet(value -> (value != -1) ? Math.min(msgID, value) : msgID);
    }

    /**
     * Aims to notify the client when all the network clients
     * have provided a response to the MESSAGE_TRANSMISSION_END request
     */
    public void notifyNetClientTransmissionEndResponse() {
        // I just need check if I have received all the response, then the client
        // will go looking for the lostMessageID that has already been populated
        // via the requestMessageResend method from the receive thread
        if(this.transmissionEndReceivedResponses.decrementAndGet() == 0)
            synchronized (this.transmissionEndReceivedResponses){
                this.transmissionEndReceivedResponses.notify();
            }
    }

    /**
     * Checks if a packet has been lost or not
     *
     * @return  True if packet is not lost, False otherwise
     */
    public boolean packetIsNotLost() {
        return this.randomGenerator.nextFloat() > this.packetLossPercentage;
    }

    //endregion

    /**
     * Starts the client.
     *
     * @param args  If specified as the first argument, the ID will be used for this client,
     *              otherwise the client ID will be taken from the client configuration file.
     **/
    public static void main(final String[] args) {
        if (args == null || args.length == 0) {
            ErrorManager.raiseError(11,"""
                    Invalid parameters.
                    Accepted parameters are (without angle brackets):
                      - <config file path>
                      - <config file path> <id>""");

            return;
        }

        try {
            if (args.length == 2)
                new ClientNode(args[0], Integer.parseInt(args[1])).run();
            else
                new ClientNode(args[0]).run();
        } catch (Exception e) {
            ErrorManager.raiseError(12, "Couldn't create the ClientNode.", e);
        }

    }
}
