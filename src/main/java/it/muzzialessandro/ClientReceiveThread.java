package it.muzzialessandro;

import it.muzzialessandro.manager.ErrorManager;
import it.muzzialessandro.manager.LogManager;
import it.muzzialessandro.model.*;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Random;

/**
 * The {@code ServerThread} class manages the interaction
 * with a network client acting as a server.
 **/
public class ClientReceiveThread implements Runnable {

    //region Private Fields

    private static final int RESEND_REQUEST_INTERVAL_MS = 3000;

    private final ClientNode clientNode;
    private final NetworkClientM netClient;
    private final StatisticM stats;
    private final int messagesToReceive;
    private int lastMessageReceived;
    private boolean waitingForResend;
    private int maxReceivedMsgID;
    private ObjectInputStream inputStream;
    private long lastSentResendRequestTime;

    //endregion

    /**
     * Class constructor.
     *
     * @param clientNode            The object representing this client
     * @param netClient             The involved network client
     * @param messagesToReceive     The number of messages I am expected to receive
     */
    public ClientReceiveThread(final ClientNode clientNode, final NetworkClientM netClient, int messagesToReceive, StatisticM stats) {

        //region Init Fields

        this.clientNode = clientNode;
        this.netClient = netClient;
        this.messagesToReceive = messagesToReceive;
        this.lastMessageReceived = 0;
        this.waitingForResend = false;
        this.maxReceivedMsgID = -1;
        this.stats = stats;

        //endregion
    }

    @Override
    public void run() {

        if(this.inputStream == null) {
            try {
                this.inputStream = new ObjectInputStream(new BufferedInputStream(this.netClient.socket.getInputStream()));
            } catch (IOException e) {
                ErrorManager.raiseError(32, "Generic error from ClientPersonalSocketThread.run(), client #${this.netClient.id}.", e);
                return;
            }
        }

        LogManager.d("Waiting for incoming messages from #${this.netClient.id}...");
        // keep checking if I have sent and received all the packets with this client
        while (true) {
            try {
                Object obj = this.inputStream.readObject();

                if (obj instanceof MessageM) {
                    if(this.clientNode.packetIsNotLost())
                        processMessage((MessageM) obj);
                    else
                        this.stats.lostMessages.incrementAndGet();
                }
                else if(obj instanceof RequestM) {
                    processRequest((RequestM) obj);
                }
                else if(obj instanceof ResponseM) {
                    processResponse((ResponseM) obj);
                }

            } catch (Exception e) {
                // check if I really need to raise an Error, I don't need to do that id:
                // - the client is closing
                // - the end of the outputStream has been reached
                if(!(this.clientNode.isClosing || (this.netClient.socket.isConnected() && e instanceof EOFException)))
                    ErrorManager.raiseError(18, "Generic error from ClientPersonalSocketThread.run().", e);
                break;
            }
        }

        LogManager.d("All messages received and sent with #${this.netClient.id}.");
    }

    //region Private Methods

    /**
     * Processes the incoming message
     *
     * @param msg   The message
     */
    private void processMessage(MessageM msg) {

        // in case someone is resending messages that I already have
        if(msg.messageID <= this.lastMessageReceived){
            return;
        }

        // in case I have missed a message and I'm not already waiting for a resend
        if(msg.messageID > (this.lastMessageReceived + 1) && !waitingForResend) {
            LogManager.d("Missed message #" + (this.lastMessageReceived + 1) + " from #${this.netClient.id}!");

            this.netClient.sendRequest(this.clientNode.clientM.id, RequestM.RequestCode.RESEND_MESSAGE, lastMessageReceived + 1);
            lastSentResendRequestTime = System.currentTimeMillis();
            waitingForResend = true;
        }
        // in case I am waiting for a resend
        else if(waitingForResend) {
            // this clause is needed to avoid sending multiple resend request "per stream"

            // if I have received the right packet
            if(msg.messageID == lastMessageReceived + 1) {
                waitingForResend = false;
                maxReceivedMsgID = -1;
                lastMessageReceived++;
                this.stats.receivedMessages.incrementAndGet();
            }
            // check if I really unluckily missed the right packet again
            // if I enter here it means that the sender has started resending the messages
            // (id lower than other already received) but I have missed the first one
            else if(msg.messageID < maxReceivedMsgID && (lastSentResendRequestTime - System.currentTimeMillis()) > RESEND_REQUEST_INTERVAL_MS) {
                this.netClient.sendRequest(clientNode.clientM.id, RequestM.RequestCode.RESEND_MESSAGE, lastMessageReceived + 1);
            }
            // simply update the new max ID received
            else {
                maxReceivedMsgID = msg.messageID;
            }
        }
        else {
            lastMessageReceived++;
            this.stats.receivedMessages.incrementAndGet();
        }
    }

    /**
     * Processes the incoming request
     *
     * @param req   The request
     */
    private void processRequest(RequestM req) {
        switch (req.code) {

            case RESEND_MESSAGE -> {
                int lostMessageID = (int) req.obj;
                this.clientNode.requestMessageResend(lostMessageID);
                LogManager.d("Client #${this.netClient.id} has missed messages from #$lostMessageID, resending missed ones to all.");
            }

            case MESSAGE_TRANSMISSION_END -> {
                if(lastMessageReceived == messagesToReceive) {
                    this.netClient.sendResponse(this.clientNode.clientM.id, req.code, ResponseM.ResponseCode.OK);
                }
                else {
                    this.netClient.sendResponse(this.clientNode.clientM.id, req.code, ResponseM.ResponseCode.ERROR, lastMessageReceived + 1);
                }
            }
        }
    }

    /**
     * Processes the incoming response to a previously sent request
     *
     * @param res   The response
     */
    private void processResponse(ResponseM res) {

        if (res.requestCode == RequestM.RequestCode.MESSAGE_TRANSMISSION_END) {
            if (res.responseCode == ResponseM.ResponseCode.ERROR) {
                this.clientNode.requestMessageResend((int) res.payload);
            }

            this.clientNode.notifyNetClientTransmissionEndResponse();
        }
    }

    //endregion
}
