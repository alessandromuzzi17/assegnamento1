package it.muzzialessandro.manager;

/**
 * Management class for errors
 */
public class LogManager {

    #define DEBUG

    /**
     * Prints a message to console
     *
     * @param msg   The message
     */
    public static void i(String msg) {
        System.out.println(msg);
    }

    /**
     * Prints a message to console if DEBUG is defined
     *
     * @param msg   The message
     */
    public static void d(String msg) {
        #if DEBUG
        System.out.println(msg);
        #endif
    }
}
