package it.muzzialessandro.manager;

import it.muzzialessandro.model.RequestM;
import it.muzzialessandro.model.ResponseM;
import it.muzzialessandro.model.MessageM;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Management class for packet sending
 */
public class PacketManager {

    // region Public Methods

    /**
     * Sends a response with code OK to the specified ObjectOutputStream
     *
     * @param os            The ObjectOutputStream
     * @param senderID      The sender ID
     * @param reqCode       The corresponding request code
     */
    public static void sendOkResponse(ObjectOutputStream os, int senderID, RequestM.RequestCode reqCode) {
        PacketManager.sendOkResponse(os, senderID, reqCode, null);
    }

    /**
     * Sends a response with code OK and the given payload to the specified ObjectOutputStream
     *
     * @param os            The ObjectOutputStream
     * @param senderID      The sender ID
     * @param reqCode       The corresponding request code
     * @param payload       The response payload
     */
    public static void sendOkResponse(ObjectOutputStream os, int senderID, RequestM.RequestCode reqCode, Serializable payload) {
        PacketManager.sendPacket(os, new ResponseM(senderID, reqCode, ResponseM.ResponseCode.OK, payload));
    }

    /**
     * Sends a response with code ERROR to the specified ObjectOutputStream
     *
     * @param os            The ObjectOutputStream
     * @param senderID      The sender ID
     * @param reqCode       The corresponding request code
     */
    public static void sendErrorResponse(ObjectOutputStream os, int senderID, RequestM.RequestCode reqCode) {
        PacketManager.sendErrorResponse(os, senderID, reqCode, null);
    }

    /**
     * Sends a response with code ERROR and the given payload to the specified ObjectOutputStream
     *
     * @param os            The ObjectOutputStream
     * @param senderID      The sender ID
     * @param reqCode       The corresponding request code
     * @param payload       The response payload
     */
    public static void sendErrorResponse(ObjectOutputStream os, int senderID, RequestM.RequestCode reqCode, Serializable payload) {
        PacketManager.sendPacket(os, new ResponseM(senderID, reqCode, ResponseM.ResponseCode.ERROR, payload));
    }

    /**
     * Sends a response with the given code to the specified ObjectOutputStream
     *
     * @param os            The ObjectOutputStream
     * @param senderID      The sender ID
     * @param reqCode       The corresponding request code
     * @param resCode       The response code
     */
    public static void sendResponse(ObjectOutputStream os, int senderID, RequestM.RequestCode reqCode, ResponseM.ResponseCode resCode) {
        PacketManager.sendResponse(os, senderID, reqCode, resCode, null);
    }

    /**
     * Sends a response with the given code and payload to the specified ObjectOutputStream
     *
     * @param os            The ObjectOutputStream
     * @param senderID      The sender ID
     * @param reqCode       The corresponding request code
     * @param resCode       The response code
     * @param payload       The response payload
     */
    public static void sendResponse(ObjectOutputStream os, int senderID, RequestM.RequestCode reqCode, ResponseM.ResponseCode resCode, Serializable payload) {
        PacketManager.sendPacket(os, new ResponseM(senderID, reqCode, resCode, payload));
    }

    /**
     * Sends a request with the given code to the specified ObjectOutputStream
     *
     * @param os            The ObjectOutputStream
     * @param senderID      The sender ID
     * @param code          The corresponding request code
     */
    public static void sendRequest(ObjectOutputStream os, int senderID, RequestM.RequestCode code) {
        PacketManager.sendRequest(os, senderID, code, null);
    }

    /**
     * Sends a request with the given code and payload to the specified ObjectOutputStream
     *
     * @param os            The ObjectOutputStream
     * @param senderID      The sender ID
     * @param code          The corresponding request code
     * @param payload       The response payload
     */
    public static void sendRequest(ObjectOutputStream os, int senderID, RequestM.RequestCode code, Serializable payload) {
        PacketManager.sendPacket(os, new RequestM(senderID, code, payload));
    }

    /**
     * Sends a message with the given ID to the specified ObjectOutputStream
     *
     * @param os            The ObjectOutputStream
     * @param senderID      The sender ID
     * @param messageID     The message ID
     */
    public static void sendMessage(ObjectOutputStream os, int senderID, int messageID) {
        PacketManager.sendPacket(os, new MessageM(senderID, messageID));
    }

    //endregion

    //region Private Methods

    /**
     * Is the only method of the class that actually send a packet, the other ones are facilities.
     * The method is {@code synchronized} in order to be thread safe.
     *
     * @param os    The ObjectOutputStream on which send (write) the packet
     * @param s     The Serializable object to send (write)
     */
    private synchronized static void sendPacket(ObjectOutputStream os, Serializable s) {
        try {
            os.writeObject(s);
            os.flush();
        } catch (IOException e) {
            ErrorManager.raiseError(21, "Can't send the requested packet.", e);
        }
    }

    //endregion
}
