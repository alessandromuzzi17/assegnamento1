package it.muzzialessandro;

import it.muzzialessandro.manager.ErrorManager;
import it.muzzialessandro.model.ClientM;
import it.muzzialessandro.model.RequestM;
import it.muzzialessandro.model.RequestM.RequestCode;
import it.muzzialessandro.manager.PacketManager;
import it.muzzialessandro.model.StatisticM;

import java.io.*;
import java.net.Socket;
import java.util.HashMap;

/**
 * The class {@code ServerThread} manages the interaction
 * with a client of the server.
 **/

public class ServerThread implements Runnable {

    //region Private Fields

    private final ServerNode serverNode;
    private final Socket clientSocket;
    private ObjectOutputStream clientOS;
    private boolean canCloseSocket;

    //endregion

    /**
     * Class constructor.
     *
     * @param master       the server.
     * @param clientSocket the client socket.
     **/
    public ServerThread(final ServerNode master, final Socket clientSocket) {
        this.serverNode = master;
        this.clientSocket = clientSocket;
        this.clientOS = null;
        this.canCloseSocket = false;
    }

    @Override
    public void run() {
        ObjectInputStream is;
        try {
            is = new ObjectInputStream(new BufferedInputStream(this.clientSocket.getInputStream()));
        } catch (Exception e) {
            ErrorManager.raiseError(5, "Can't get the socket's InputStream.", e);
            return;
        }

        while (!this.canCloseSocket) {
            try {
                Object i = is.readObject();

                if (i instanceof RequestM) {
                    RequestM rq = (RequestM) i;

                    ensureOutputStreamIsNotNull();

                    switch (rq.code) {
                        case REGISTRATION -> performRegistration(rq);
                        case STATISTICS -> manageStatistics(rq);
                        default -> ErrorManager.raiseWarning(44, "Request not allowed. Request code ${rq.code}");
                    }
                }
            } catch (Exception e) {
                if(!(e instanceof EOFException))
                    ErrorManager.raiseError(10, "Generic error from ServerThread.run().", e);
            }
        }

        try {
            this.clientSocket.close();
        } catch (IOException e) {
            ErrorManager.raiseError(40, "Unable to close the client socket.", e);
        }

        if (this.serverNode.getPool().getActiveCount() == 1)
        {
          this.serverNode.close();
        }
    }

    //region Private Methods

    private void performRegistration(RequestM rq) {
        if (rq.obj instanceof ClientM) {
            this.serverNode.registerClient((ClientM) rq.obj);
            PacketManager.sendOkResponse(this.clientOS, ServerNode.SERVER_ID, rq.code);
            return;
        }

        PacketManager.sendErrorResponse(this.clientOS, ServerNode.SERVER_ID, rq.code, "The given object is not a ClientM instance.");
    }

    private void manageStatistics(RequestM rq) {
        if(rq.obj instanceof StatisticM) {
            this.serverNode.addNewStatisticAndWaitForAll((StatisticM) rq.obj);
            PacketManager.sendOkResponse(this.clientOS, ServerNode.SERVER_ID, rq.code);
            this.canCloseSocket = true;
            return;
        }

        PacketManager.sendErrorResponse(this.clientOS, ServerNode.SERVER_ID, rq.code, "The given object is not a StatisticM instance.");
    }

    //region Utility Methods

    private void ensureOutputStreamIsNotNull() {
        if (this.clientOS == null) {
            try {
                this.clientOS = new ObjectOutputStream(new BufferedOutputStream(this.clientSocket.getOutputStream()));
            } catch (IOException e) {
                ErrorManager.raiseError(9, "Can't get the socket's OutputStream.", e);
            }
        }
    }

    //endregion

    //endregion

    //region Public Methods

    public void informClientAboutNetwork(HashMap<Integer, ClientM> networkMap) {
        ensureOutputStreamIsNotNull();
        PacketManager.sendRequest(this.clientOS, ServerNode.SERVER_ID, RequestCode.NETWORK_INFO, networkMap);
    }

    //endregion
}
