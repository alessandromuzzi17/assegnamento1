package it.muzzialessandro;

import it.muzzialessandro.manager.ConfigurationManager;
import it.muzzialessandro.manager.ErrorManager;

import java.nio.file.Paths;

public class Simulation {

    private final int numOfClients;

    /**
     * Class constructor.
     *
     * @param xmlConfigPath The XML configuration file path
     */
    public Simulation(String xmlConfigPath) {
        ConfigurationManager configManager = ConfigurationManager.importConfigurationFromFile(xmlConfigPath);

        this.numOfClients = Integer.parseInt(configManager.getFromConfig("clients_number"));
    }

    public void run() throws Exception {

        Runtime runtime = Runtime.getRuntime();

        System.out.println("Starting Maven compilation...");
        Process mavenCompile = runtime.exec("mvn clean install");
        mavenCompile.waitFor();
        System.out.println("Maven compilation completed.");

        System.out.println("Launching server node...");
        Thread serverThread = new Thread(() -> ServerNode.main(new String[]{"src/main/resources/it/muzzialessandro/server_config.xml"}));
        serverThread.start();

        System.out.println("Launching client nodes...");
        String input2 = Paths.get("").toAbsolutePath() + "/target/ClientNode-jar-with-dependencies.jar";
        for (int i = 0; i < numOfClients; i++) {
            String cmd_to_run_client = "java -jar \"$input2\" src/main/resources/it/muzzialessandro/client_config.xml $i";

#if OS_MAC
            String[] command = new String[] {"/bin/sh", "-c", cmd_to_run_client};
#elif OS_WINDOWS
            String[] command = new String[] {"cmd.exe", "/c", cmd_to_run_client};
#else
            String[] command = new String[] {};
            ErrorManager.raiseError(38, "OS not supported.");
#endif

            runtime.exec(command);
            Thread.sleep(200);
        }

        serverThread.join();
    }

    public static void main(final String[] args) {
        if (args == null || args.length == 0) {
            ErrorManager.raiseError(47, """
                    Invalid parameters.
                    Accepted parameters are (without angle brackets):
                      - <config file path>""");

            return;
        }

        try {
            new Simulation(args[0]).run();
        } catch (Exception e) {
            ErrorManager.raiseError(48, "Couldn't create the Simulation.", e);
        }
    }
}
