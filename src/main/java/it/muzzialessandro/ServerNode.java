package it.muzzialessandro;

import it.muzzialessandro.manager.ConfigurationManager;
import it.muzzialessandro.manager.ErrorManager;
import it.muzzialessandro.manager.LogManager;
import it.muzzialessandro.manager.PacketManager;
import it.muzzialessandro.model.ClientM;
import it.muzzialessandro.model.RequestM;
import it.muzzialessandro.model.StatisticM;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ServerNode {

    //region Public Fields

    public static final int SERVER_ID = -50;

    //endregion

    //region Private Fields

    private static final int CORE_POOL = 10;
    private static final int MAX_POOL = 100;
    private static final long IDLE_TIME = 5000;

    private final String outputCsvPath;
    private final ServerSocket socket;
    private final ThreadPoolExecutor pool;
    private final Thread cmdThread;
    private final HashMap<Integer, ClientM> clients;
    private final ArrayList<ServerThread> clientThreads;
    private final ArrayList<StatisticM> stats;
    private boolean canAcceptClients;
    private boolean canAcceptCommands;
    private boolean isClosing;

    //endregion

    /**
     * Class constructor.
     *
     * @throws Exception If the creation of the server socket fails.
     */
    public ServerNode(String xmlConfigPath) throws Exception {
        //region Init Config

        ConfigurationManager configManager = ConfigurationManager.importConfigurationFromFile(xmlConfigPath);
        int masterPort = Integer.parseInt(configManager.getFromConfig("master_port"));
        String csvPath = configManager.getFromConfig("output_csv_path");

        //endregion

        //region Init Fields

        this.outputCsvPath = csvPath;
        this.socket = new ServerSocket(masterPort);
        this.clients = new HashMap<>();
        this.clientThreads = new ArrayList<>();
        this.canAcceptCommands = true;
        this.canAcceptClients = true;
        this.pool = new ThreadPoolExecutor(CORE_POOL, MAX_POOL, IDLE_TIME, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
        this.isClosing = false;
        this.stats = new ArrayList<>();

        //endregion

        //region Console Commands

        this.cmdThread = new Thread(() -> {
            String helpMsg = """
                Commands list:
                  - start:  start client messages exchange
                  - stop:   stop the master node
                  - help:   print this message
                  """;
            LogManager.i(helpMsg);

            String cmd;
            Scanner consoleScanner = new Scanner(System.in);
            try {
                while (this.canAcceptCommands) {
                    cmd = consoleScanner.nextLine().trim();

                    switch (cmd) {
                        case "start" -> {
                            this.canAcceptClients = false;
                            startClientsActivity();
                        }
                        case "stop" -> {
                            this.canAcceptCommands = false;
                            this.close();
                            System.exit(0);
                        }
                        default -> LogManager.i(helpMsg);
                    }
                }
            }
            catch (Exception e) {
                // ignore, needed only to suppress exception
                // raised from System.in.close() when it's time to interrupt console.readline()
                this.canAcceptCommands = false;
            }
        });

        this.cmdThread.start();

        //endregion
    }

    /**
     * Runs the server code.
     **/
    private void run() {
        LogManager.i("Waiting for clients to join...");

        //region New Clients Management

        while (true) {
            try {
                Socket s = this.socket.accept();

                if (this.canAcceptClients) {
                    ServerThread th = new ServerThread(this, s);
                    clientThreads.add(th);
                    this.pool.execute(th);

                    LogManager.i("New client arrived. There are " + this.pool.getTaskCount() + " clients at the moment.");
                } else {
                    LogManager.i("Sorry, no more clients accepted.");
                    ObjectOutputStream rejectedOS = new ObjectOutputStream(new BufferedOutputStream(s.getOutputStream()));
                    PacketManager.sendErrorResponse(rejectedOS, SERVER_ID, RequestM.RequestCode.REGISTRATION, "Sorry, no more clients accepted.");
                }

            } catch (Exception e) {
                if(!this.isClosing)
                    ErrorManager.raiseError(14, "Couldn't correctly accept the new client.", e);
                break;
            }
        }

        //endregion

        this.pool.shutdown();

        //region Elaborate Statistics

        statistics statsCSV = statistics.create();

        // sort stats by clientID
        this.stats.sort(Comparator.comparingInt(e -> e.clientID));
        int messagesToSend = this.stats.get(0).messagesToSend;
        // this is needed in order to normalize the data and get the percentages
        double percentageCoeff = messagesToSend * (this.clients.size() - 1);

        // add stats to csv for each client
        this.stats.forEach(stat -> {
            statsCSV.add(statistics.statisticsItem.builder()
                    .withId(stat.clientID)
                    .withExecutionTime(LocalTime.ofNanoOfDay(stat.executionTimeMS*1000000))
                    .withSentMessages(stat.sentMessages)
                    .withSentMessagesPercentage(roundValues(stat.sentMessages / percentageCoeff, 2))
                    .withResentMessages(stat.resentMessages)
                    .withResentMessagesPercentage(roundValues(stat.resentMessages / percentageCoeff, 2))
                    .withReceivedMessages(stat.receivedMessages.get())
                    .withReceivedMessagesPercentage(roundValues(stat.receivedMessages.get() / percentageCoeff, 2))
                    .withLostMessages(stat.lostMessages.get())
                    .withLostMessagesPercentage(roundValues(stat.lostMessages.get() / percentageCoeff, 2))
                    .build());
        });

        // add summary stats to csv
        double totalPercentageCoeff = percentageCoeff * this.clients.size();
        long totalExecutionTimeMS = this.stats.stream().mapToLong(it -> it.executionTimeMS).sum();
        int totalSentMsg = this.stats.stream().mapToInt(it -> it.sentMessages).sum();
        int totalResentMsg = this.stats.stream().mapToInt(it -> it.resentMessages).sum();
        int totalReceivedMsg = this.stats.stream().mapToInt(it -> it.receivedMessages.get()).sum();
        int totalLostMsg = this.stats.stream().mapToInt(it -> it.lostMessages.get()).sum();
        statsCSV.add(statistics.statisticsItem.builder()
                .withId(-1)
                .withExecutionTime(LocalTime.ofNanoOfDay(totalExecutionTimeMS*1000000/this.clients.size()))
                .withSentMessages(totalSentMsg)
                .withSentMessagesPercentage(roundValues(totalSentMsg / totalPercentageCoeff, 2))
                .withResentMessages(totalResentMsg)
                .withResentMessagesPercentage(roundValues(totalResentMsg / totalPercentageCoeff, 2))
                .withReceivedMessages(totalReceivedMsg)
                .withReceivedMessagesPercentage(roundValues(totalReceivedMsg / totalPercentageCoeff, 2))
                .withLostMessages(totalLostMsg)
                .withLostMessagesPercentage(roundValues(totalLostMsg / totalPercentageCoeff, 2))
                .build());

        try {
            String csvFilePath = this.outputCsvPath + "/statistics_${messagesToSend}.csv";
            if(!Files.exists(Path.of(csvFilePath))) {
                new File(csvFilePath);
            }

            FileWriter myWriter = new FileWriter(csvFilePath);
            myWriter.write(statsCSV.write().toCsv());
            myWriter.close();
        } catch (IOException e) {
            ErrorManager.raiseError(45, "Error writing the statistics CSV file.");
        }

        //endregion

        LogManager.i("Execution completed successfully!");
        try {
            System.in.close();
        } catch (IOException e) {
            ErrorManager.raiseError(49, "Error while trying to close System.in.");
        }
    }

    //region Private Methods

    /**
     * Communicates to start to all the registered clients
     */
    private void startClientsActivity() {
        clientThreads.forEach(it -> it.informClientAboutNetwork(clients));
    }

    /**
     * Rounds a value keeping {@code numOfDigits} digits after comma
     *
     * @param value         The value to be rounded
     * @param numOfDigits   The number of digits after the comma
     * @return              The rounded value with {@code numOfDigits} digits after comma
     */
    private double roundValues(double value, int numOfDigits) {
        double coeff = Math.pow(10, numOfDigits);
        return Math.round(value * coeff) / coeff;
    }

    //endregion

    //region Public Methods

    /**
     * Registers the given client
     *
     * @param clientM   The client to register
     */
    public void registerClient(@NotNull ClientM clientM) {
        this.clients.put(clientM.id, clientM);
    }

    /**
     * Adds the given statistics to the already collected ones and waits for all the statistics to be received
     *
     * @param stat  The statistics to collect
     */
    public void addNewStatisticAndWaitForAll(StatisticM stat) {
        synchronized (this.stats) {
            this.stats.add(stat);

            // check if I got the statistics from all the clients
            if(this.stats.size() != this.clients.size()){
                try {
                    this.stats.wait();
                } catch (InterruptedException e) {
                    ErrorManager.raiseError(43, "Error while waiting for other stats to arrive");
                }
            }
            else
                this.stats.notifyAll();
        }
    }

    /**
     * Gets the server pool.
     *
     * @return the thread pool.
     **/
    public ThreadPoolExecutor getPool() {
        return this.pool;
    }

    /**
     * Closes the server execution.
     **/
    public void close() {
        try {
            this.isClosing = true;
            this.socket.close();
        } catch (Exception e) {
            ErrorManager.raiseError(13, "Exception closing master's socket.", e);
        } finally {
            this.cmdThread.interrupt();
        }
    }

    //endregion


    /**
     * Starts the MasterNode.
     *
     * @param args The node XML configuration file path is passed as first parameter
     **/
    public static void main(final String[] args) {
        if (args == null || args.length == 0) {
            ErrorManager.raiseError(1, """
                    Invalid parameters.
                    Accepted parameters are (without angle brackets):
                      - <config file path>""");

            return;
        }

        try {
            new ServerNode(args[0]).run();
        } catch (Exception e) {
            ErrorManager.raiseError(4, "Couldn't create the MasterNode.", e);
        }
    }
}
